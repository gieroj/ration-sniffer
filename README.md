We can run below comand to see errors

vendor/bin/phpcs --standard=vendor/rotary-international/ration-sniffer/phpcs.xml

Or we can try fix errors automatically by:

vendor/bin/phpcbf ./folder/or/file_name.php
