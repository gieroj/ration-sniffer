<?php

namespace RotaryInternational\Web\Sniffer;

use Zend\Mvc\Application as ZendApplication;
use Zend\Stdlib\ArrayUtils;

class Application extends ZendApplication
{

    /** 
     * Provide default module configuration.
     *
     * These are the modules that every ration application
     * is going to need in order to be an "application".
     */
    public static function getDefaultModules()
    {
        return [
            'Zend\Router',
            'Zend\Session',
            'Zend\Validator'
        ];
    }

    /**
     * Override of the "init" method provided by Zend\Mvc\Application.
     *
     * Stuff more configuration into the config array that is provided by
     * loading configuration files.
     */
    public static function init($configuration = [])
    {
        $defaultModules = self::getDefaultModules();
        if (isset($configuration['modules'])) {
            $configuration['modules'] = ArrayUtils::merge($configuration['modules'], self::getDefaultModules());
        } else {
            $configuration['modules'] = self::getDefaultModules();
        }

        return ZendApplication::init($configuration);
    }
}
